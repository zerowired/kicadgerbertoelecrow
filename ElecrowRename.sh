#!/bin/sh

rm -rf ./elecrow_gerber
cp -r ./gerber ./elecrow_gerber
cd ./elecrow_gerber

for org in `find ./ -name '*-F.Cu.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-F\.Cu\.gbr/\.GTL/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*-B.Cu.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-B\.Cu\.gbr/\.GBL/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*-F.Mask.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-F\.Mask\.gbr/\.GTS/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*-B.Mask.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-B\.Mask\.gbr/\.GBS/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*-F.SilkS.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-F\.SilkS\.gbr/\.GTO/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*-B.SilkS.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-B\.SilkS\.gbr/\.GBO/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*-Edge.Cuts.gbr' | sed 's/\.\/\///g'`
do
    ele=`echo ${org} | sed -e 's/-Edge\.Cuts\.gbr/\.GML/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

for org in `find ./ -name '*\.drl' | sed 's/\.\/\///g' `
do
    ele=`echo ${org} | sed -e 's/\.drl/\.TXT/g'`
    echo ${org} to be ${ele}
    mv ./${org} ./${ele}
done

cd ../
zip -r elecrow_gerber.zip ./elecrow_gerber

